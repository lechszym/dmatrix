/* 
 * File:   Matrix.cpp
 * Author: lechszym
 * 
 * Created on 14 April 2015, 9:08 PM
 */

#include "Matrix.h"
#include <stdint.h>

namespace dmatrix {
      
   Matrix::Matrix() 
      : MatrixBase() {}

   Matrix::Matrix(double *data, size_t nRows, size_t nCols, enum CBLAS_ORDER order) 
      :MatrixBase(data, nRows, nCols, order) {}
   
   
   Matrix::Matrix(size_t nRows, size_t nCols, enum CBLAS_ORDER order) 
      :MatrixBase(nRows, nCols, order) {}

   Matrix::Matrix(const MatrixBase &m, size_t rowIdx, size_t colIdx, size_t nRows, size_t nCols) 
      : MatrixBase(m, rowIdx, colIdx, nRows, nCols) {}

   Matrix::Matrix(const MatrixBase &m, size_t nRows, size_t nCols) 
      : MatrixBase(m, nRows, nCols) {}
   
   
   Matrix::Matrix(const MatrixBase& m) 
      : MatrixBase(m, 0, 0, m.rows, m.cols) {}

   Matrix::~Matrix() {}

   double& Matrix::operator()(size_t i, size_t j) {
      return this->get(i,j);
   }
   
   
   void Matrix::set_identity() {
      if (rows != cols) {
         std::wcout << "Error! Can't create identity " << rows << "x" << cols <<
                 " matrix!!!\n";
         exit(-1);
      }

      double *data = this->data();
      size_t i = 0;
      this->set_all(0.0);
      while (i < rows * cols) {
         data[i] = 1.0;
         i += _tda + 1;
      }

   }

   Matrix Matrix::transpose() const {
       Matrix m(*this);
       m.t();
       return m;
   }
    
   
   Matrix& Matrix::operator+=(double x) {
      this->elementwise(*this,*this,x,[](double dst, double src, double y) -> double { return dst+y; });
      return (*this);
   }

   Matrix Matrix::operator+(double x) {
      Matrix result(rows,cols,_order);
      this->elementwise(result,*this,x,[](double dst, double src, double y) -> double { return src+y; });
      return result;
   }
   
   
   Matrix& Matrix::operator-=(double x) {
      this->elementwise(*this,*this,x,[](double dst, double src, double y) -> double { return dst-y; });
      return (*this);
   }
   
   Matrix Matrix::operator-(double x) {
      Matrix result(rows,cols,_order);
      this->elementwise(result,*this,x,[](double dst, double src, double y) -> double { return src-y; });
      return result;
   }   

   Matrix& Matrix::operator*=(double x) {
      this->elementwise(*this,*this,x,[](double dst, double src, double y) -> double { return dst*y; });
      return (*this);
   }

   Matrix Matrix::operator*(double x) {
      Matrix result(rows,cols,_order);
      this->elementwise(result,*this,x,[](double dst, double src, double y) -> double { return src*y; });
      return result;
   }   
   
   Matrix& Matrix::operator/=(double x) {
      this->elementwise(*this,*this,x,[](double dst, double src, double y) -> double { return dst/y; });
      return (*this);
   }

   Matrix Matrix::operator/(double x) {
      Matrix result(rows,cols,_order);
      this->elementwise(result,*this,x,[](double dst, double src, double y) -> double { return src/y; });
      return result;      
   }

   
   Matrix& Matrix::operator+=(const Matrix& m) {
      this->elementwise(*this,m,0.0,[](double dst, double src, double y) -> double { return dst+src; });
      return (*this);
   }
   
   Matrix Matrix::operator+(const Matrix& m) {
      Matrix result = this->copy();
      this->elementwise(result,m,0.0,[](double dst, double src, double y) -> double { return dst+src; });
      return result;      
   }   

   Matrix& Matrix::operator-=(const Matrix& m) {
      this->elementwise(*this,m,0.0,[](double dst, double src, double y) -> double { return dst-src; });
      return (*this);
   }
   
   Matrix Matrix::operator-(const Matrix& m) {
      Matrix result = this->copy();
      this->elementwise(result,m,0.0,[](double dst, double src, double y) -> double { return dst-src; });
      return result;      
   }   
   
   void Matrix::mul_elements(const Matrix& m) {
      this->elementwise(*this,m,0.0,[](double dst, double src, double y) -> double { return dst*src; });      
   }

   void Matrix::div_elements(const Matrix& m) {
      this->elementwise(*this,m,0.0,[](double dst, double src, double y) -> double { return dst/src; });      
   }

   void Matrix::copy(const Matrix& m) {
      this->elementwise(*this,m,0.0,[](double dst, double src, double y) -> double { return src; });
   }      

   
   Matrix Matrix::copy() const {
      Matrix result(rows,cols,_order);
      result.copy(*this);
      return (result);
   }
   
   void Matrix::dgemm(double alpha, const Matrix& A, const Matrix& B, double beta, Matrix &C) {
      
       if ((A.rows != C.rows) || (B.cols != C.cols) || (A.cols != B.rows)) {
         std::wcout << "Error! Can't do multiply " << A.rows << "x" << A.cols <<
                 " matrix by a " << B.rows << "x" << B.cols << 
                 " matrix and store result in a " << C.rows << "x" << C.cols <<
                 " matrix!!!\n";
         exit(-1);
      }   
      
      Matrix At,Bt;
      const Matrix *Ap,*Bp;
      enum CBLAS_TRANSPOSE TransA, TransB;
      int K;
      
      if(A._order == C._order) {
         TransA = CblasNoTrans;
         Ap = &A;
         K = Ap->cols;
      } else {
         TransA = CblasTrans;
         At = A.transpose();
         Ap = &At;
         K = Ap->rows;
      }
      
      if(B._order == C._order) {
         TransB = CblasNoTrans;
         Bp = &B;
      } else {
         TransB = CblasTrans;
         Bt = B.transpose();
         Bp = &Bt;
      }
      
      cblas_dgemm (C._order, TransA, TransB, C.rows, C.cols, K, alpha, Ap->data(), Ap->_tda, Bp->data(), Bp->_tda, beta, C.data(), C._tda);
   }

   Matrix Matrix::operator*(const Matrix& m) {
      Matrix result(rows,m.cols);
      Matrix::dgemm(1.0,*this,m,0.0,result);
      return result;      
   }   
   
   Vector Matrix::row(size_t i) {
      Matrix m = Matrix(*this,i,0,1,cols);     
      Vector result(m,0,cols);
      return result;
   }

   Vector Matrix::column(size_t j) {
      Matrix m = Matrix(*this,0,j,rows,1);
      Vector result(m,0,rows);
      return result;
   }
}
