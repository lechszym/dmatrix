/* 
 * File:   MatrixBase.cpp
 * Author: lechszym
 * 
 * Created on 22 April 2015, 9:22 AM
 */

#include "MatrixBase.h"

#include <stdint.h>
#include <stdlib.h>
#include <memory>

#ifdef MATLAB_MEX_FILE
char dl_mexErrorStr[1024];
#endif

//static int allocs=0;
//static int deletes = 0;

namespace dmatrix {
      
   //template<typename T>
   struct myDeleter
   {
       void operator() (double*& ptr)
       {
           if (ptr)
           {
               //free(ptr);
               //ptr=NULL;
              //std::cout << "(" << ++deletes << ")Deleting " << ptr << "\n";
              
              
              delete[] ptr;
           }
       }
   };   
   
   MatrixBase::MatrixBase() {
      rows = 0;
      cols = 0;
      _rowi = 0;
      _coli = 0;
      _tda = 0;
      _weakdata = NULL;
   }

   MatrixBase:: MatrixBase(double *data, size_t nRows, size_t nCols, enum CBLAS_ORDER order) : MatrixBase() {
      _weakdata = data;
      
      rows = nRows;
      cols = nCols;
      _order = order;
      if (_order == CblasRowMajor) {
         _tda = nCols;
      } else {
         _tda = nRows;
      }
   }
   
   
   MatrixBase::MatrixBase(size_t nRows, size_t nCols, enum CBLAS_ORDER order) : MatrixBase() {
      _data = data_ptr(new double[nRows * nCols],std::default_delete<double[]>());
      //double *tmp = new double[nRows * nCols];
      //std::cout << "(" << ++allocs << ")Allocating " << tmp << "\n"; 
      //_data = data_ptr(tmp,myDeleter());
      
      if (!_data.get()) {
#ifdef MATLAB_MEX_FILE
         sprintf(dl_mexErrorStr, "Error! Failed to allocate a %ux%u matrix!!!", 
                 (unsigned int) nRows, (unsigned int) nCols);
         mexErrMsgTxt(dl_mexErrorStr);         
#else         
         std::wcout << "Error! Failed to allocate a " << nRows << "x" << nCols
                 << " matrix!!!\n";
         exit(-1);
#endif
      }

      rows = nRows;
      cols = nCols;
      _order = order;
      if (_order == CblasRowMajor) {
         _tda = nCols;
      } else {
         _tda = nRows;
      }
   }

   MatrixBase::MatrixBase(const MatrixBase &m, size_t rowIdx, size_t colIdx, size_t nRows, size_t nCols) {
      submatrix(m, rowIdx, colIdx, nRows, nCols);
   }

   MatrixBase::MatrixBase(const MatrixBase &m, size_t nRows, size_t nCols) {
      if(m.rows == 1) {
         if(m._tda < m.cols) {
#ifdef MATLAB_MEX_FILE
         sprintf(dl_mexErrorStr, "Error! Can't create a %ux%u matrix from non-contiguous %ux%u vector!!!", 
                 (unsigned int) nRows, (unsigned int) nCols,
                 (unsigned int) m.rows, (unsigned int) m.cols);
         mexErrMsgTxt(dl_mexErrorStr);         
#else            
            std::wcout << "Error! Can't create a " << nRows << "x" << nCols <<
                 " matrix from non-contiguous " << m.rows << "x" << m.cols <<
                 " vector!!!\n";
            exit(-1);
#endif
         }
      } else if(m.cols == 1) {
         if(m._tda < m.rows) {
#ifdef MATLAB_MEX_FILE            
         sprintf(dl_mexErrorStr, "Error! Can't create a %ux%u matrix from non-contiguous %ux%u vector!!!", 
                 (unsigned int) nRows, (unsigned int) nCols,
                 (unsigned int) m.rows, (unsigned int) m.cols);
         mexErrMsgTxt(dl_mexErrorStr);         
#else         
            std::wcout << "Error! Can't create a " << nRows << "x" << nCols <<
                 " matrix from non-contiguous " << m.rows << "x" << m.cols <<
                 " vector!!!\n";
            exit(-1);
#endif
         }         
      } else {
#ifdef MATLAB_MEX_FILE
         sprintf(dl_mexErrorStr, "Error! Can't create a %ux%u matrix from non-vector %ux%u matrix!!!", 
                 (unsigned int) nRows, (unsigned int) nCols,
                 (unsigned int) m.rows, (unsigned int) m.cols);
         mexErrMsgTxt(dl_mexErrorStr);                  
#else
         std::wcout << "Error! Can't create a " << nRows << "x" << nCols <<
                 " matrix from non-vector " << m.rows << "x" << m.cols <<
                 " matrix!!!\n";
         exit(-1);
#endif         
      }

      _order = m._order;
      _data = data_ptr(m._data);
      _weakdata = m._weakdata;
      if (_order == CblasRowMajor) {
         _tda = nCols;
      } else {
         _tda = nRows;
      }
       _rowi = m._rowi;
       _coli = m._coli;
      rows = nRows;
      cols = nCols;   
   }
   
   MatrixBase::MatrixBase(const MatrixBase& m) {
      submatrix(m, 0, 0, m.rows, m.cols);
   }

   MatrixBase::~MatrixBase() {

   }

   void MatrixBase::submatrix(const MatrixBase &m, size_t rowIdx, size_t colIdx, size_t nRows, size_t nCols) {

      if ((rowIdx + nRows) > m.rows || (colIdx + nCols) > m.cols) {
#ifdef MATLAB_MEX_FILE
         sprintf(dl_mexErrorStr, "Error! Can't create a %ux%u submatrix at (%u,%u) out of a %ux%u matrix!!!", 
                 (unsigned int) nRows, (unsigned int) nCols, 
                 (unsigned int) rowIdx, (unsigned int) colIdx,
                 (unsigned int) m.rows, (unsigned int) m.cols);
         mexErrMsgTxt(dl_mexErrorStr);
#else         
         std::wcout << "Error! Can't create a " << nRows << "x" << nCols <<
                 " submatrix at (" << rowIdx << "," << colIdx << ") out of a " <<
                 m.rows << "x" << m.cols << " matrix!!!\n";
         exit(-1);
#endif         
      }

      _order = m._order;
      _data = data_ptr(m._data);
      _weakdata = m._weakdata;
      _tda = m._tda;
       _rowi = m._rowi + rowIdx;
       _coli = m._coli + colIdx;
      rows = nRows;
      cols = nCols;   
   }
   
   void MatrixBase::set_all(double x) {
      double *data = this->data();
      size_t I, J;
      if (_order == CblasRowMajor) {
         I = rows;
         J = cols;
      } else {
         I = cols;
         J = rows;
      }

      for (size_t i = 0; i < I; i++) {
         for (size_t j = 0; j < J; j++) {
            data[j] = x;
         }
         data += _tda;
      }
   }

   void MatrixBase::set_zero() {
      this->set_all(0.0);
   }

   void MatrixBase::t() {
      if(_order == CblasRowMajor) {
           _order = CblasColMajor;
       } else {
           _order = CblasRowMajor;
       }
       size_t temp = cols;
       cols = rows;
       rows = temp;

       temp = _coli;
       _coli = _rowi;
       _rowi = temp;
   }
   
   
   std::string MatrixBase::toString() const {
      std::stringstream ss;

      ss.precision(4);

      ss << "[";
      for (size_t i = 0; i < rows; i++) {
         for (size_t j = 0; j < cols; j++) {
            ss << this->get(i, j);
            if(j < (cols-1)) {
               ss << ", ";
            }
         }
         if(i < (rows-1)) {
            ss << ";\n ";
         }
      }
      ss << "]\n";
      return ss.str();
   }
}
