/* 
 * File:   Vector.h
 * Author: lechszym
 *
 * Created on 14 April 2015, 9:09 PM
 */

#ifndef VECTOR_H
#define	VECTOR_H

#include "MatrixBase.h"

namespace dmatrix {

class Matrix;
   
class Vector : public MatrixBase {
public:
   Vector();
   Vector(size_t n, enum CBLAS_ORDER order=CblasColMajor);
   Vector(const MatrixBase &v, size_t offset, size_t n);

   Vector(const Vector& orig);
   virtual ~Vector();
   
   double& operator()(size_t i);
   double get(size_t i) const;
   void set(size_t i, double x);
   
   void copy(const Vector& m);
   Vector copy() const;  
   
   Vector& operator+=(double x);
   Vector operator+(double x);

   Vector& operator-=(double x);
   Vector operator-(double x);

   Vector& operator*=(double x);
   Vector operator*(double x);

   Vector& operator/=(double x);
   Vector operator/(double x);

   void mul_elements(const Vector& v);
   void div_elements(const Vector& v);

   Vector& operator+=(const Vector& v);
   Vector operator+(const Vector& v);

   Vector& operator-=(const Vector& v);
   Vector operator-(const Vector& v);   
   
   static double dot(const Vector& x, const Vector& y);
   double dot(const Vector& v) const;


   static void axpy(double alpha, const Vector& x, Vector& y);
   void axpy(double alpha, const Vector& x);
   Vector operator*(const Vector &v) const;
   
   double norm() const;
   size_t idamax() const;
   
   size_t size;
   
private:
   inline size_t stride() const {
      if(_order == CblasColMajor && cols == 1) {
         return 1;
      } else if(_order == CblasRowMajor && rows == 1) {
         return 1;
      } else {
         return _tda;
      }
   }
   
};

double sum(Vector& v);

}
#endif	/* Vector_H */

