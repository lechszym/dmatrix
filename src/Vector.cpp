/* 
 * File:   Vector.cpp
 * Author: lechszym
 * 
 * Created on 14 April 2015, 9:09 PM
 */

#include "Vector.h"
#include <iostream>

namespace dmatrix {

#ifdef MATLAB_MEX_FILE
char dl_mexErrorStr[1024];
#endif
   
Vector::Vector() : MatrixBase() {
   size = 0;
}

Vector::Vector(size_t n,enum CBLAS_ORDER order) : MatrixBase(n,1,CblasColMajor) {
   if(order == CblasRowMajor) {
      this->t();
   }
   size = n;
}

Vector::Vector(const MatrixBase &v, size_t offset, size_t n) {
   //if(v.order() == CblasColMajor) {
   //   submatrix(v,offset,0,n,1);
   //} else {
   //   submatrix(v,0,offset,1,n);
   //}
   if(v.rows == 1) {
      submatrix(v,0,offset,1,n);
   } else if(v.cols == 1) {
      submatrix(v,offset,0,n,1);
   } else {
#ifdef MATLAB_MEX_FILE
         sprintf(dl_mexErrorStr, "Error! Can't subvector a %ux%u matrix (one dim must be equal to 1)!!!", 
                 (unsigned int) v.rows, (unsigned int) v.cols);
         mexErrMsgTxt(dl_mexErrorStr);
#else         
         std::wcout << "Error! Can't subvector a " << v.rows << "x" << v.cols <<
                 " matrix (one dim must be equal to 1)!!!\n";
         exit(-1);

#endif
   }
   
   size = n;
}

Vector::Vector(const Vector& v) : MatrixBase(v, 0, 0, v.rows, v.cols) {
   size = v.size;
}

Vector::~Vector() {
}

double& Vector::operator()(size_t i) {
   if(rows == 1) {
      return MatrixBase::get(0,i);
   } else {
      return MatrixBase::get(i,0);
   }
}

double Vector::get(size_t i) const {
   if(rows == 1) {
      return MatrixBase::get(0,i);
   } else {
      return MatrixBase::get(i,0);
   }
}

void Vector::set(size_t i, double x) {
   if(rows == 1) {
      MatrixBase::set(0,i,x);
   } else {
      MatrixBase::set(i,0,x);
   }
}


   void Vector::copy(const Vector& m) {
      this->elementwise(*this,m,0.0,[](double dst, double src, double y) -> double { return src; });
   }      

   
   Vector Vector::copy() const {
      Vector result(size,_order);
      result.copy(*this);
      return result;
   }

   Vector& Vector::operator+=(double x) {
      this->elementwise(*this,*this,x,[](double dst, double src, double y) -> double { return dst+y; });
      return (*this);
   }

   Vector Vector::operator+(double x) {
      Vector result(size,_order);
      this->elementwise(result,*this,x,[](double dst, double src, double y) -> double { return src+y; });
      return result;
   }
   
   
   Vector& Vector::operator-=(double x) {
      this->elementwise(*this,*this,x,[](double dst, double src, double y) -> double { return dst-y; });
      return (*this);
   }
   
   Vector Vector::operator-(double x) {
      Vector result(size,_order);
      this->elementwise(result,*this,x,[](double dst, double src, double y) -> double { return src-y; });
      return result;
   }   

   Vector& Vector::operator*=(double x) {
      this->elementwise(*this,*this,x,[](double dst, double src, double y) -> double { return dst*y; });
      return (*this);
   }

   Vector Vector::operator*(double x) {
      Vector result(size,_order);
      this->elementwise(result,*this,x,[](double dst, double src, double y) -> double { return src*y; });
      return result;
   }   
   
   Vector& Vector::operator/=(double x) {
      this->elementwise(*this,*this,x,[](double dst, double src, double y) -> double { return dst/y; });
      return (*this);
   }

   Vector Vector::operator/(double x) {
      Vector result(size,_order);
      this->elementwise(result,*this,x,[](double dst, double src, double y) -> double { return src/y; });
      return result;      
   }

   Vector& Vector::operator+=(const Vector& v) {
      this->elementwise(*this,v,0.0,[](double dst, double src, double y) -> double { return dst+src; });
      return (*this);
   }
   
   Vector Vector::operator+(const Vector& v) {
      Vector result = this->copy();
      this->elementwise(result,v,0.0,[](double dst, double src, double y) -> double { return dst+src; });
      return result;      
   }   

   Vector& Vector::operator-=(const Vector& v) {
      this->elementwise(*this,v,0.0,[](double dst, double src, double y) -> double { return dst-src; });
      return (*this);
   }
   
   Vector Vector::operator-(const Vector& v) {
      Vector result = this->copy();
      this->elementwise(result,v,0.0,[](double dst, double src, double y) -> double { return dst-src; });
      return result;      
   }   
   
   void Vector::mul_elements(const Vector& v) {
      this->elementwise(*this,v,0.0,[](double dst, double src, double y) -> double { return dst*src; });      
   }

   void Vector::div_elements(const Vector& v) {
      this->elementwise(*this,v,0.0,[](double dst, double src, double y) -> double { return dst/src; });      
   }   

   double Vector::dot(const Vector& x, const Vector& y) {
      if (x.size != y.size) {
         std::wcout << "Error! Can't do inner product of " << x.size << 
                 "-element vector with a " << y.size << "-element vector!!!\n";
         exit(-1);
      }
      return cblas_ddot(x.size, x.data(), x.stride(), y.data(), y.stride());
   }
 
   double Vector::dot(const Vector& v) const {
      return Vector::dot(*this,v);
   }

   void Vector::axpy(double alpha, const Vector &x, Vector &y) {
      if (x.size != y.size) {
         std::wcout << "Error! Can't axpy a " << x.size << 
                 "-element vector with a " << y.size << "-element vector!!!\n";
         exit(-1);
      }
      cblas_daxpy(x.size, alpha, x.data(), x.stride(), y.data(), y.stride());
   }

   void Vector::axpy(double alpha, const Vector& x) {
      Vector::axpy(alpha, x, *this);
   }
   
   Vector Vector::operator*(const Vector &v) const {
      Vector result(size);
      result.copy(*this);
      Vector::axpy(1.0, v, result);
      return result;
   }
   
   double Vector::norm() const {
      return cblas_dnrm2 (size, this->data(), this->stride()); 
   }
   
    size_t Vector::idamax() const {
      return cblas_idamax (size, this->data(), this->stride()); 
    }

    
    double sum(Vector& v) {
       double J=0.0;
       for(int i=0; i<v.size; i++) {
          J += v(i);
       }
       return J;
    }
    
}
