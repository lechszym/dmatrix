/* 
 * File:   Matrix.h
 * Author: lechszym
 *
 * Created on 14 April 2015, 9:08 PM
 */

#ifndef MATRIX_H
#define	MATRIX_H


#include "MatrixBase.h"
#include "Vector.h"

namespace dmatrix {
  
class Vector;
   
class Matrix : public MatrixBase {
public:
 
   Matrix();
   Matrix(double *data, size_t nRows, size_t nCols, enum CBLAS_ORDER order=CblasColMajor);
   Matrix(size_t nRows, size_t nCols, enum CBLAS_ORDER order=CblasColMajor);
   Matrix(const MatrixBase& m, size_t rowIdx, size_t colIdx, size_t nRows, size_t nCols);
   Matrix(const MatrixBase &m, size_t nRows, size_t nCols);
   Matrix(const MatrixBase& m);
   virtual ~Matrix();

   double& operator()(size_t i, size_t j);
   
   void set_identity();

   Matrix transpose() const;

   Matrix& operator+=(double x);
   Matrix& operator-=(double x);
   Matrix& operator*=(double x);
   Matrix& operator/=(double x);

   Matrix& operator+=(const Matrix& m);
   Matrix& operator-=(const Matrix& m);
   void mul_elements(const Matrix& m);
   void div_elements(const Matrix& m);

   Matrix operator+(double x);
   Matrix operator-(double x);
   Matrix operator*(double x);
   Matrix operator/(double x);

   Matrix operator+(const Matrix& m);
   Matrix operator-(const Matrix& m);
   
   static void dgemm(double alpha, const Matrix& A, const Matrix& B, double beta, Matrix &C);

   Matrix operator*(const Matrix& m);

   void copy(const Matrix &m);
   Matrix copy() const;

   Vector row(size_t i);
   Vector column(size_t j);
   
   
private:

};
}

#endif	/* MATRIX_H */

