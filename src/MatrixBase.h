/* 
 * File:   MatrixBase.h
 * Author: lechszym
 *
 * Created on 22 April 2015, 9:15 AM
 */

#ifndef MATRIXBASE_H
#define  MATRIXBASE_H

#include <stdlib.h>
#include <sstream>
#include <iostream>

#ifdef OPENBLAS_ENABLED
#include <cblas.h>
#else
#include <gsl/gsl_blas.h>
#endif

#ifdef BOOST_ENABLED
#include <boost/shared_ptr.hpp>
#else
#include <memory>
#endif

#ifdef MATLAB_MEX_FILE
#include "mex.h"
#endif

namespace dmatrix {

#ifdef BOOST_ENABLED
typedef boost::shared_ptr<double> data_ptr;    
#else
typedef std::shared_ptr<double> data_ptr;
#endif

class MatrixBase {
public:
 
   MatrixBase();
   MatrixBase(double *data, size_t nRows, size_t nCols, enum CBLAS_ORDER order=CblasColMajor);
   MatrixBase(size_t nRows, size_t nCols, enum CBLAS_ORDER order=CblasColMajor);
   MatrixBase(const MatrixBase& m, size_t rowIdx, size_t colIdx, size_t nRows, size_t nCols);
   MatrixBase(const MatrixBase& m, size_t nRows, size_t nCols);
   MatrixBase(const MatrixBase& m);
   virtual ~MatrixBase();

   
   inline double* data() const {
      double* data;
      if(!_weakdata) {
         data = _data.get();
      } else {
         data = _weakdata;
      }
      if (_order == CblasColMajor) {
         return data + _coli * _tda + _rowi;
      } else {
         return data + _rowi * _tda + _coli;
      }
   }
   
   inline double& get(size_t i, size_t j) const {
      double *data = this->data();
      if (_order == CblasRowMajor) {
         return data[_tda * i + j];
      } else {
         return data[_tda * j + i];
      }
   }

   inline void set(size_t i, size_t j, double x) {
      double *data = this->data();
      if (_order == CblasRowMajor) {
         data[_tda * i + j] = x;
      } else {
         data[_tda * j + i] = x;
      }
   }

   inline enum CBLAS_ORDER order() const {
      return _order;
   };

   void set_all(double x);
   void set_zero();
   
   template<typename Lambda>
   static void elementwise(MatrixBase &dst, const MatrixBase &src, double x, Lambda op) { // or Lambda&&, which is usually better
      if ((src.rows != dst.rows) || (src.cols != dst.cols)) {

#ifdef MATLAB_MEX_FILE
         mexErrMsgTxt("Here1");
#else         
         std::wcout << "Error! Can't do elementwise operation on a " << dst.rows << "x" << dst.cols <<
                 " matrix with a " << src.rows << "x" << src.cols << " matrix!!!\n";
         exit(-1);
#endif  
      }      
      
      double *dataDst = dst.data();

      size_t I, J;

      if (dst._order == CblasRowMajor) {
         I = dst.rows;
         J = dst.cols;
      } else {
         I = dst.cols;
         J = dst.rows;
      }

      if (dst._order == src._order) {
         double *dataSrc = src.data();
         for (size_t i = 0; i < I; i++) {
            for (size_t j = 0; j < J; j++) {
               dataDst[j] = op(dataDst[j],dataSrc[j],x);
            }
            dataDst += dst._tda;
            dataSrc += src._tda;
         }
      } else if (dst._order == CblasRowMajor) {
         for (size_t i = 0; i < I; i++) {
            for (size_t j = 0; j < J; j++) {
               dataDst[j] = op(dataDst[j],src.get(i, j),x);
            }
            dataDst += dst._tda;
         }
      } else {
         for (size_t i = 0; i < I; i++) {
            for (size_t j = 0; j < J; j++) {
               dataDst[j] = op(dataDst[j],src.get(j, i),x);
            }
            dataDst += dst._tda;
         }
      }
   }      

   std::string toString() const;   
   
   friend std::ostream& operator<<(std::ostream &out, const MatrixBase &m) {
      out << m.toString();
      return out;
   }
   
   size_t   rows;
   size_t   cols;
   
   
protected:
   void submatrix(const MatrixBase &m, size_t rowIdx, size_t colIdx, size_t nRows, size_t nCols);
   void t();


   data_ptr        _data;
   size_t          _rowi;
   size_t          _coli;
   size_t          _tda;
   double *         _weakdata;
   enum CBLAS_ORDER _order;
};
}

#endif	/* MATRIXBASE_H */

