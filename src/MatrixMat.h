/* 
 * File:   MatrixMat.h
 * Author: lechadmin
 *
 * Created on September 7, 2015, 2:47 PM
 */

#ifndef MATRIXMAT_H
#define	MATRIXMAT_H

#include "Matrix.h"
#include "mex.h"

namespace dmatrix {


   Matrix MatToMatrix(const mxArray *mx) {
      int M = mxGetM(mx); 
      int N = mxGetN(mx);
      double *dmx = mxGetPr(mx);
      
      // Should check if double matrix
      return Matrix(dmx, M, N, CblasColMajor);
   }

};

#endif	/* MATRIXMAT_H */

